/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula01;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author katharinep
 */
public class Aula01 {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(2300);
        Socket sock = server.accept();
        InputStream in = sock.getInputStream();
        OutputStream out = sock.getOutputStream();
        byte buf[] = new byte [10];
        int tam = in.read(buf);
        String msg = new String(buf,0,tam);
        System.out.println(msg);
    }
    
}

