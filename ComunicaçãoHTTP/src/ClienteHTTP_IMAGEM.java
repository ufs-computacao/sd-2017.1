import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClienteHTTP {
	public static void main (String args[]) throws UnknownHostException, IOException{
		Socket socket = new Socket("www.ufs.br",80);
		
		InputStream in= socket.getInputStream();
		OutputStream out = socket.getOutputStream();
		
		String sep = "\r\n"; //Separador de mensagem
		String msg = "GET /uploads/image/path/103/Institu_Pano.jpg HTTP/1.1" + sep +
				"Host:www.ufs.br"+sep+sep;			
		out.write(msg.getBytes());
		
		InputStreamReader isr = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(isr);
		
		
		String resp = br.readLine();
		System.out.println(resp);
		while ((resp = br.readLine()).length() > 0){
			System.out.println(resp);
		}
		
		File file = new File("pano.jpg");
		FileOutputStream fos = new FileOutputStream(file);
		byte[] buf = new byte[1024];
		int size;
		while((size=in.read(buf)) > -1){
			fos.write(buf, 0, size);
		}
		fos.flush();
		fos.close();
		socket.close();
	}
}
