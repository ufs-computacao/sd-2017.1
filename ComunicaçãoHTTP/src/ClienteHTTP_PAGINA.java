import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClienteHTTP {
	public static void main (String args[]) throws UnknownHostException, IOException{
		Socket socket = new Socket("www.ufs.br",80);
		
		InputStream in= socket.getInputStream();
		OutputStream out = socket.getOutputStream();
		
		String sep = "\r\n"; //Separador de mensagem
		String msg = "GET / HTTP/1.1" + sep +"Host:www.ufs.br"+sep+sep;		
		out.write(msg.getBytes());
		
		
		byte[] buf = new byte[1024];
		int size;
		while((size = in.read(buf)) > 0 ){
		String resp = new String(buf,0,size);
		System.out.println(resp);
		}
		System.out.println(size);
	}
}
