import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.util.Scanner;

public class Send {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    //Objeto entrada de dados construtor
    Scanner reader = new Scanner(System.in); 
    //Armazenar variavel em Strings:
    String inputLine;
    //Variavel auxiliar:
    boolean flag = true;
    
    //Autentica��o de Conex�o:
    factory.setHost("wasp.rmq.cloudamqp.com");
    factory.setUsername("dblgjcho");
    factory.setPassword("oPQKgc5BKqJnx8qIEn9u7VwbwQDVUuQd");
    factory.setVirtualHost("dblgjcho");
    
    while (flag == true) {
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    
    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    //Entrada de mensagens:
    inputLine = reader.nextLine(); 
    channel.basicPublish("", QUEUE_NAME, null, inputLine.getBytes("UTF-8"));
    if (inputLine.isEmpty() == true) {
    	flag = false;
    }else{
    System.out.println(" [x] Sent '" + inputLine + "'");
    }
    //Fechando o canal de conversa��o:
    channel.close();
    connection.close();
    }
    reader.close();

  }
}